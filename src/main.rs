#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]
/**
 * $File: main.rs $
 * $Date: 2024-05-22 14:27:46 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */

fn main() {
    if_let::main();
    //match_expr_combine_option_enum::main();
    //matching_pattern::main();
    //option_enum::main();
    //try_enum::main();
}

mod if_let {
    pub fn main() {
        let some_value = Some(3);
        match some_value {
            Some(3) => println!("three"),
            _ => (),
        }

        if let Some(3) = some_value {
            println!("three");
        }
    }
}

mod match_expr_combine_option_enum {
    pub fn main() {
        let five = Some(5);
        let six = plus_one(five);
        let none = plus_one(None);
    }

    pub fn plus_one(x: Option<i32>) -> Option<i32> {
        match x {
            Some(i) => Some(i + 1),
            _ => None,
        }
    }
}

mod matching_pattern {
    pub fn main() {
        value_in_cents(Coin::Quarter(UsState::Alaska));
    }

    // File
    #[derive(Debug)]
    enum UsState {
        Alabama,
        Alaska,
        Arizona,
        Arkansas,
        California,
    }

    enum Coin {
        Penny,
        Nickel,
        Dime,
        Quarter(UsState),
    }

    fn value_in_cents(coin: Coin) -> u8 {
        match coin {
            Coin::Penny => 1,
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter(state) => {
                println!("State quater from {:?}!", state);
                25
            }
        }
    }
}

mod option_enum {
    pub fn main() {
        let x: i8 = 5;
        let y: Option<i8> = Some(5);

        let sum = x + y.unwrap_or(0);
    }
}

mod try_enum {
    pub fn main() {
        let localhost = IpAddrKind::V4(127, 0, 0, 1);
    }

    enum Message {
        Quit,
        Move { x: i32, y: i32 },
        Write(String),
        ChangeColor(i32, i32, i32),
    }

    impl Message {
        fn some_function() {
            println!("Let's Get Rusty!");
        }
    }

    enum IpAddrKind {
        V4(u8, u8, u8, u8),
        V6(String),
    }

    struct IpAddr {
        kind: IpAddrKind,
        address: String,
    }

    fn route() {}
}
